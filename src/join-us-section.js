export function addJoinUsSection(headerText, buttonText) {

  const subTitleText = 'Sed do eiusmod tempor incididunt \nut labore et dolore magna aliqua.';

  const section = createElement('section', ['app-section', 'join-program-section']);
  addNewElement(section, 'h2', 'app-title', headerText);
  addNewElement(section, 'h3', 'app-subtitle', subTitleText);

  section.appendChild(createJoinUsSectionForm(buttonText));

  const app = document.getElementById('app-container');
  const footer = document.getElementsByClassName('app-footer')[0];
  app.insertBefore(section, footer);
}

export function removeJoinUsSection() {
  const section = document.getElementsByClassName('join-program-section')[0];
  section.parentNode.removeChild(section);
}

function createJoinUsSectionForm(submitButtonText) {
  const form = createElement('form', 'join-program-section__form');
  const input = createElement('input', 'join-program-section__email');
  input.setAttribute('type', 'text');
  input.setAttribute('placeholder', 'Email');
  form.appendChild(input);
  addNewElement(form, 'button', ['app-section__button', 'join-program-section__button'], submitButtonText);

  form.addEventListener('submit', (e) => {
    e.preventDefault();
    console.log(document.querySelector('.join-program-section__email').value);
  });

  return form;

}

function createElement(elemType, classNames, innerText) {
  const elem = document.createElement(elemType);

  Array.isArray(classNames) ? elem.classList.add(...classNames) : elem.classList.add(classNames);

  if (innerText) {
    elem.innerText = innerText;
  }

  return elem;
}

function addNewElement(parentElem, elemType, classNames, innerText) {
  parentElem.appendChild(createElement(elemType, classNames, innerText));
}

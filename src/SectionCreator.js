import { addJoinUsSection, removeJoinUsSection } from './join-us-section.js';

export class SectionCreator{
  create(type) {
    switch (type) {
      case 'advanced':
        return new Section('Join Our Advanced Program', 'Subscribe to Advanced Program');
      case 'standard':
      default:
        return new Section('Join Our Program', 'Subscribe');
    }
  }
}

class Section {
  constructor(headerText, buttonText) {
    this.headerText = headerText;
    this.buttonText = buttonText;
  }

  render() {
    addJoinUsSection(this.headerText, this.buttonText);
  }

  remove() {
    removeJoinUsSection();
  }
}

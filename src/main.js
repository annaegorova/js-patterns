import { SectionCreator } from './SectionCreator.js';

document.addEventListener('DOMContentLoaded', () => {
  new SectionCreator().create('standard').render();
});
